#!/usr/bin/perl

# This file is part of Mana.
#
# Mana is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

use Modern::Perl;
use Dancer2;
use Dancer2::Serializer::JSON;
use Text::CSV;

=head1 NAME

json_to_csv.pl - convert a JSON formatted resource into a CSV

=head1 SYNOPSIS

perl json_to_csv.pl resourcename filename

=head1 DESCRIPTION

Convert a json file formatted as follows into a csv.
The JSON has to contain IN THE SAME LINE the two keys:
securitytoken: the securitytoken used to share
[ resourcename ]: an array of resources.

=head1 ARGUMENTS

=over

=item resourcename

The name of the resource you want to convert

=item filename

The file you want to convert

=head1 RETURNED VALUE

The returned value is not consistent.

=cut

my $resourceName = $ARGV[0];
my $filename = $ARGV[1];

open(my $fh, '<:encoding(UTF-8)', $filename) or die "Could not open file '$filename'";
my $json = from_json(<$fh>);
my @list = @{ $json->{ $resourceName."s" } };

my $destname = $filename;
$destname =~ s/\.json/\.csv/;
open(my $fh2, '>:encoding(UTF-8)', $destname) or die "Could not open file '$destname'";
my $csv = Text::CSV->new({ eol => "$/"});
my @columns = keys %{ $list[0] };
$csv->column_names( \@columns );
$csv->print( $fh2, \@columns );
foreach my $line ( @list ) {
    $csv->print_hr( $fh2, $line ); 
}
close $fh;
