#!/usr/bin/perl

# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

=head1 NAME

export_subscriptions_for_mana.pl - Export subscription to Mana format.

=head1 SYNOPSIS

./ export_subscriptions_for_mana.pl -l fr-FR

=head1 DESCRIPTION

This script export all subscription into a CSV file intended
to be imported into Mana server.

=cut

use Modern::Perl;
use Getopt::Long;
use C4::Context;
use Pod::Usage;
use Text::CSV;

my ($help, $language, $verbose);
GetOptions(
    'help|?'        => \$help,
    'language|l:s'  => \$language,
    'verbose|v'     => \$verbose,
) or pod2usage(2);

$language ||= 'en';

=head1 NAME

export_subscriptions_for_mana.pl - Export subscription to Mana format.

=head1 SYNOPSIS

export_subscriptions_for_mana.pl
  [ -l ]

=head1 OPTIONS

=over 8

=item B<--help>

Print a brief help message and exits.

=item B<--language>

Subscription language that will be specified into Mana.

=back
=cut

my $dbh = C4::Context->dbh;
my $sth = $dbh->prepare("
    SELECT b.title, bi.issn, bi.ean, bi.publishercode, sf.description AS sfdescription,
    sf.unit, sf.unitsperissue, sf.issuesperunit, sn.description,
    sn.label, sn.numberingmethod, sn.label1, sn.add1, sn.every1, sn.whenmorethan1, sn.setto1, sn.numbering1,
    sn.label2, sn.add2, sn.every2, sn.whenmorethan2, sn.setto2, sn.numbering2,
    sn.label3, sn.add3, sn.every3, sn.whenmorethan3, sn.setto3, sn.numbering3
    FROM subscription s
    LEFT JOIN biblio b ON b.biblionumber = s.biblionumber
    LEFT JOIN subscription_frequencies sf ON sf.id = s.periodicity
    LEFT JOIN biblioitems bi ON bi.biblionumber = s.biblionumber
    LEFT JOIN subscription_numberpatterns sn ON sn.id = s.numberpattern") or die $dbh->errstr;

my @headers = ("title","issn","ean","publishercode","sfdescription","unit",
    "unitsperissue","issuesperunit","label","sndescription",
    "numberingmethod","label1","add1","every1","whenmorethan1",
    "setto1","numbering1","label2","add2","every2","whenmorethan2",
    "setto2","numbering2","label3","add3","every3","whenmorethan3",
    "setto3","numbering3","nbofusers","lastimport","creationdate",
    "exportemail","kohaversion","language");

my $content = join(',', @headers) . "\n";

my %versions = C4::Context::get_versions();

$sth->execute;
my $results = $sth->fetchall_arrayref({});
foreach my $s ( @$results ) {
    $s->{kohaversion} = $versions{'kohaVersion'};
    $s->{language} = $language;
    foreach my $header ( @headers ) {
        $s->{ $header } ||= '';
        $content .= '"' . $s->{ $header } . '",';
    }
    $content =~ s/\,$/\n/;
}
print $content;
