package Mana;

use Modern::Perl;

use Dancer2;
use Dancer2::Plugin::REST;
use Dancer2::Plugin::Database;
use Dancer2::Plugin::Passphrase;
use Dancer2::Plugin::Auth::Tiny;
use Dancer2::Serializer::JSON;
use DateTime;
use utf8;
use constant POST_TOKEN_EXCEPTIONS => qw( /getsecuritytoken /confirmaccount /delete );
#put in POST_TOKEN_EXCEPTIONS all post methods which do not need security token
use constant RESOURCE_CHECK_EXCEPTIONS_EXACT => qw ( / /getsecuritytoken /registration /confirmaccount);
#put in RESOURCE_CHECK_EXCEPTIONS_EXACT all routes (exact name) which do not need the parameter resource to be checked
use constant RESOURCE_CHECK_EXCEPTIONS_LIKE => qw ( /getsuggestion );
#put in RESOURCE_CHECK_EXCEPTIONS_LIKE all routes (partial name) which do not need the parameter resource to be checked

use Mana::Resource;
use Mana::Interface;

set serializer => 'JSON';
set session => "Simple";
set layout => "main";

our $VERSION = '1.0';

prepare_serializer_for_format;
hook before => sub {
    # if the request is a post request which needs a security token ( all except routes specified in POST_TOKEN_EXCEPTION ).
    if ( request->is_post
            and (not grep { request->path eq $_ } POST_TOKEN_EXCEPTIONS)
    ){
        #we check the token
        my $token = params->{securitytoken};
        my $librarian;
        $librarian =  database->quick_select( "librarian", { id => $token } ) if $token;
        # activationdate determines if the token is active and usable
        unless ( $librarian and $librarian->{ activationdate } ){
            halt( status_unauthorized({ msg => "Invalid security token given, if you are on Koha, please check your syspref ManaToken. Given security token: $token"}) );
        }
    }
    #if the route needs a precise resource to exist and be accessible..
    unless ( grep { request->path =~ $_ } RESOURCE_CHECK_EXCEPTIONS_LIKE
             or  grep { request->path eq $_ } RESOURCE_CHECK_EXCEPTIONS_EXACT ) {
        unless (Mana::Resource::isValidResource(params->{resource})) {
            # ..we check it is in the authorized resources list
            my $msg = "The resource: ".params->{resource} . " can't be found";
            halt( status_bad_request( { msg => $msg } ));
        }
    }
};

=head1 NAME

Mana - Mana Routes module, centralize all routes.

=head1 ROUTES

=head2 Hook after

=head3 NAME

Hook after - Not direcly callable

=cut

=head3 DESCRIPTION

=head4 Behaviour

    The hook after checks that the functions ended correctly and returned the statuscode like /2../. Otherwise, it sets the status of Dancer2's answer to the returned status

=cut

hook after => sub {
    my $response = shift;
    my $status;
    eval { $status = from_json( $response->{content} )->{statuscode} };
    unless ( $@ or (not $status ) ){
        #if the route returned a statuscode and a message
        status( $status );
    }
    my $filename = config->{log_path}."production.log";
    open(my $fh, '>>:encoding(UTF-8)', $filename ) or die "Could not open file '$ARGV[0]'";
    chmod 0660, $fh;
    my $logmsg = "executing request from: ".request->remote_address.". Calling route: ".request->path_info.". Status returned:  ";
    if ( $status ){
        $logmsg.=$status.".";
    }
    else{
        $logmsg.=200 . ".";
    }
    if ( $response->{content} ){
        $logmsg.= " Content size: ". length($response->{content}). " chars."
    };
    $logmsg.="\n";
    print $fh $logmsg;
    close $fh;
};

=head2 GET /

=head3 SYNOPSIS

    GET / - Index route

=cut

get '/' => sub {
    my $response = {
        title => 'Mana KB API',
	description => 'Mana KB is a global knowledge base for library-centric data.
                        It has been designed initially to interact with Koha, the Open
                        Source ILS, but can be used by any other software.',
        version => $VERSION
    };
    return to_json($response);
};

=head2 GET /:resource/:id.:format

=head3 SYNOPSIS

    GET /:resource/:id.:format - get a specific resource

=cut

=head3 DESCRIPTION

=head4 Behaviour

    This route is part of the mana API service.
    It receives a resource name and an id and search in the corresponding table the resource with this id

=cut

=head4 Returned values

    It returns a hash with keys: data, msg and statuscode.
    - statuscode 200, no msg, and data => all worked as expected
    - statuscode 500, "Error : [ database error code ], no data => an error occured on the database.

=cut


get '/:resource/:id.:format' => sub {
    Mana::Resource::getEntity({ params });
};

=head2 /:resource.:format

=head3 SYNOPSIS

=head4 Name

    GET /:resource.:format - searches a resource matching with params.

=head4 Params

        - if the resource is a report -> query containing words to search in reports KB.
        - else -> a hash containing keys from @valid_params content in the corresponding module in lib/Mana/Resource/

=cut


=head3 DESCRIPTION

=head4 Behaviour

    This route is part of the mana API service.
    It receives a resource name and specific informations related.

=cut

=head4 Returned values

    It returns a hash with keys: data, msg and statuscode.
    - statuscode 200, no msg, data full => all worked as expected
    - statuscode 500, "Error : [ database error code ], no data => an error occured on the database.

=cut


get '/:resource.:format' => sub {
    my $result = Mana::Resource::search({ params });
};

=head2 /:resource./id.format/increment/:field

=head3 SYNOPSIS

=head4 Name

    POST /:resource/id.format/increment/:field - Increment a numeric field of a resource (most of the time: # of users using a resource)

=head4 Params

        - step:  integer containing the incrementation step.
        - securitytoken:  string given by mana to authenticate.

=head3 DESCRIPTION

=head4 Behaviour

    This route is part of the mana API service.
    This route increment a specific field from a specific resource. The resource is designed with his id, the field is the name of the field in database.
    If the number of user is incremented, that means someone imported it recently and it updates the field lastimport

=head4 Returned values

    It returns a hash with keys: data, msg and statuscode.
    - statuscode 200, no msg, some data => all worked as expected   
    - statuscode 500, "Error : [ database error code ], no data => an error occured on the database, happends mostly when the specified field is not present/not a number in the database.

=cut

post '/:resource/:id.:format/increment/:field' => sub {
    Mana::Resource::incrementField({ params });
};

=head2 /:resource.:format (POST)

=head3 SYNOPSIS

=head4 Name

    POST /:resource.:format - Share (submit) one resource to Mana.

=head4 Parameters

        one hash containing:
            - resource: the resource name
            - securitytoken: the security token given by mana
            - all keys necessary to create a new data (see @required_entry in the related resource in lib/Mana/Resource/ )

=head3 DESCRIPTION

=head4 Behaviour

    This route is part of the mana API service.
    This route creates a new resource with informations specified in parameters. It checks if there are no duplicates and generates a lot of informations in the database.
    This route can be used to comment a resource by specifying 'resource_comment' and by giving necessary parameters.

=head4 Returned values

    It returns a hash with keys: id, msg and statuscode.
    Success:
        - statuscode 201, msg: "created successfuly", id. The id returned is the id of the created resource in the database.
        - statuscode 208, msg: "resource already exists", id.
    Fail
        - statuscode 500, msg: "Error : [ database error code ]", no data => an error occured on the database, happends mostly when the specified field is not present/not a number in the database.
        - statuscode 406, msg: "Invalid Entry on [ resource ]", no data => some fields are missing, see @required_entry in the related resource in lib/Mana/Resource

=cut

post '/:resource.:format' => sub {
    my $resource = params->{resource};
    my $content = from_json(request->body, { utf8 => 1});

    return Mana::Resource::postEntity($resource, $content);
};

=head2 /bulk/:resource.:format

=head3 SYNOPSIS

    POST /bulk/:resource.:format - share (upload) a lot of informations to mana

=cut

=head3 DESCRIPTION

=head4 Behaviour

    This route is part of the mana API service.
    It receives a couple of resources and store them as a file which will be stored in the database through 'misc/cronjob/import_from_files.sh'.
    For performances reasons, bulk data are not stored in real time, but with the cronjob. The cron is run once a day (2AM, Western Europe, Paris TZ)

=head4 Returned values

     statuscode: 200 -> upload successful
     statuscode: 500 -> file couldn't be opened (shouldn't occur)

=cut



post '/bulk/:resource.:format' => sub {
    my $resource_name = params->{ resource };
    return status_406({ msg => "This resource doesn't exist or can't be imported bulk"}) unless ( Mana::Resource->isBulkImportableResource( $resource_name ) );
    my $securitytoken = params->{ securitytoken };
    my $datapath = config->{data_path};
        #file is [manadata]/[resourcename]/[date].[securitytoken].json
    my $filename = $datapath. "/". $resource_name. "/" . DateTime->now->ymd()."-" . DateTime->now->hms("-") . "." . $securitytoken . ".json";
#$filename=$datapath."test.json";
    open(my $fh, '>:encoding(UTF-8)', $filename) or die "Could not open file '$filename'";
    chmod 0660, $fh;
    print $fh request->content;
    close $fh;
    status_ok();
};

=head2 /getsecuritytoken

=head3 SYNOPSIS

=head4 Name

    POST /getsecuritytoken - ask mana for a Security Token

=head4 Parameters

    - firstname, lastname, email: informations for the token

=head3 DESCRIPTION

=head4 Behaviour

    This route is part of the mana registration service on database.
    This is one of the two only post methods which can be called without security token.
    It stores personal informations in the database and generates a security Token which is used to counter spamm.
    It sends an e-mail to the user with the address $manaddress/registration?id=$token  in order to let him validate the account.

=head4 Returned values

    status: 201 or 500.
    token: the created token, also sent by e-mail. 

=cut


post '/getsecuritytoken' => sub {
    my $status = Mana::Interface::sendConfirmation({ params });
    if ($status->{token}) {
        status_created();
    }
    return to_json( $status );
};

get '/getsuggestion/:id/:idtype' => sub{
    my $result = Mana::Resource::Reading_pair->getSuggestion( params->{id}, params->{idtype}, params->{offset}, params->{length} );
    return to_json({ data => $result, total => scalar @$result });
};

true;
